<?php
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	 	$string = $_POST['string'];
	 	
	 	function asc2bin($in)#syntax - asc2bin("text to convert");
		{
		 $out = '';
		 for ($i = 0, $len = strlen($in); $i < $len; $i++)
		 {
		$out .= sprintf("%08b",ord($in{$i}));
		 }
		 return $out;
		}

		$ascii2bin = asc2bin($string);
		$firstEncrypt = crc32($ascii2bin);
		$Hashing = hash('sha512', $firstEncrypt);
		$final_encrypt = $Hashing;
	 } 
?>
<!DOCTYPE html>
<html>
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Hash Generator</title>
		<link rel="stylesheet" type="text/css" href="plugins/css/bootstrap.min.css">
		<style type="text/css">
			.container {
				text-align: center;
				vertical-align: middle;
			}
		</style>
	</head>
	<body>
		<div class="container">
			<h3>Hashing Generator</h3>
			<hr/>
			<form class="form-inline" method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
				<div class="form-group">
					<input type="text" class="form-control" name="string" placeholder="String" /><br/><br/><br/>
					<textarea class="form-control" readonly="readonly"><?php if(isset($final_encrypt)) { echo $final_encrypt; } ?></textarea>
				</div>
			</form>
		</div>
	</body>
</html>