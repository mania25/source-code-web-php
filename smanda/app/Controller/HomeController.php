<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppController', 'Controller');

/**
 * CakePHP HomeController
 * @author mania25
 */
class HomeController extends AppController {
    public $helpers = array('Html', 'Form', 'Js' => array('Jquery'), 'Paginator');
    public $components = array('Session', 'RequestHandler', 'Paginator');

    public function index() {
        $this->layout = "home";
        $this->modelClass = "Berita";
        $this->paginate = array(
            'limit' => 3,
            'recursive' => 0,
            'order' => array('tgl_agenda' => 'DESC')
        );
        $this->set('berita', $this->paginate('Berita'));
        if ($this->RequestHandler->isAjax()) {
            $this->autoRender = FALSE;
            $this->layout = NULL;
            if ($this->RequestHandler->isAjax()) {
                $this->render('ajax_index', 'ajax');
            }
        }
    }
    
    public function directory_siswa() {
        $this->layout = "home";
        $this->modelClass = "Siswa";
        $this->loadModel('Jurusan');
        $this->paginate = array(
            'limit' => 1,
            'recursive' => 0,
            'order' => array('nama' => 'ASC')
        );
        $jurusan = $this->Jurusan->find('list', array("fields" => array("id_jurusan", "nama_jurusan")));
        $this->set('jurusan', $jurusan);
        $this->set('siswa', $this->paginate('Siswa'));
        if ($this->RequestHandler->isAjax()) {
            $this->autoRender = FALSE;
            $this->layout = NULL;
            if ($this->RequestHandler->isAjax()) {
                $this->render('ajax_dir_siswa', 'ajax');
            }
        }
    }
    
    public function search_siswa() {
        $this->modelClass = "Siswa";
        $jurusan = $this->request->data['Siswa']['id_jurusan'];
        $nama = $this->request->data['Siswa']['nama'];
        $this->paginate = array(
            'limit' => 1,
            'recursive' => 0,
            'order' => array('nama' => 'ASC'),
            'conditions' => array('Siswa.id_jurusan' => $jurusan, 'AND' => array('Siswa.nama LIKE' => '%'.$nama.'%'))
        );
        $this->set('siswa', $this->paginate('Siswa'));
        if ($this->RequestHandler->isAjax()) {
            $this->autoRender = FALSE;
            $this->layout = NULL;
            if ($this->RequestHandler->isAjax()) {
                $this->render('ajax_dir_siswa', 'ajax');
            }
        }
    }
    
    public function directory_guru() {
        $this->layout = "home";
        $this->modelClass = "Guru";
        $this->paginate = array(
            'limit' => 5,
            'recursive' => 0,
            'order' => array('nama' => 'ASC')
        );
        $this->set('guru', $this->paginate('Guru'));
        if ($this->RequestHandler->isAjax()) {
            $this->autoRender = FALSE;
            $this->layout = NULL;
            if ($this->RequestHandler->isAjax()) {
                $this->render('ajax_dir_guru', 'ajax');
            }
        }
    }
    
    public function search_guru() {
        $this->modelClass = "Guru";
        $jabatan = $this->request->data['Guru']['jabatan'];
        $nama = $this->request->data['Guru']['nama'];
        $this->paginate = array(
            'limit' => 1,
            'recursive' => 0,
            'order' => array('nama' => 'DESC'),
            'conditions' => array('jabatan' => $jabatan, 'AND' => array('nama LIKE' => '%'.$nama.'%'))
        );
        $this->set('guru', $this->paginate('Guru'));
        if ($this->RequestHandler->isAjax()) {
            $this->autoRender = FALSE;
            $this->layout = NULL;
            if ($this->RequestHandler->isAjax()) {
                $this->render('ajax_dir_guru', 'ajax');
            }
        }
    }


    public function directory_alumni() {
        $this->layout = "home";
        $this->modelClass = "Alumni";
        $this->set('alumni', $this->Alumni->find('all'));
    }
}
