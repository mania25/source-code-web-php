<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppController', 'Controller');

/**
 * CakePHP DashboardsController
 * @author mania25
 */
class DashboardsController extends AppController {
    public $helpers = array('Html', 'Form', 'Js' => array('Jquery'), 'Paginator');
    public $components = array('Session', 'RequestHandler', 'Paginator');

    private function setCurrentPage($name) {
        $this->set('currentPage', $name);
    }

    public function index() {
        $this->layout = "dashboard";
        $this->setCurrentPage("Dashboards");
    }
    
    public function berita() {
        $this->layout = "dashboard";
        $this->modelClass = "Berita";
        $this->setCurrentPage("Berita");
        $this->paginate = array(
            'limit' => 10,
            'recursive' => 0,
            'order' => array('tgl_agenda' => 'DESC')
        );
        $this->set('berita', $this->paginate('Berita'));
        if ($this->RequestHandler->isAjax()) {
            $this->autoRender = FALSE;
            $this->layout = NULL;
            if ($this->RequestHandler->isAjax()) {
                $this->render('ajax_berita', 'ajax');
            }
        }
    }
    
    public function add_berita() {
        $this->modelClass = "Berita";
        if ($this->request->is('post')) {
            $this->Berita->create();
            $this->Berita->set('tgl_agenda', date("Y-m-d"));
            $this->Berita->set('id_author', 1);
            if ($this->Berita->save($this->request->data)) {
                $this->Session->setFlash("Berita Berhasil Di Posting", "flash_success");
                return $this->redirect(array('action' => 'berita'));
            }
            $this->Session->setFlash(__("Berita Gagal Di Posting"), "flash_error");
        }
    }
    
    public function edit_berita() {
        $this->modelClass = "Berita";
        $id = $this->request->data['Berita']['id'];
        
        $berita = $this->Berita->findById($id);
        if (!$berita) {
            throw new NotFoundException(__('Berita tidak ditemukan'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $this->Berita->Id = $id;
            $this->Berita->set('tgl_agenda', date("Y-m-d"));
            $this->Berita->set('id_author', 1);
            if($this->Berita->save($this->request->data)) {
                $this->Session->setFlash("Berita Berhasil Di Edit", "flash_success");
                return $this->redirect(array('action' => 'berita'));
            }
            $this->Session->setFlash("Berita Gagal Di Edit", "flash_error");
        }
    }
    
    public function delete_berita($id) {
        $this->modelClass = "Berita";
        if ($this->Berita->delete($id)) {
            $this->Session->setFlash(
                __('Berita telah berhasil dihapus'), "flash_success"
            );
            return $this->redirect(array('action' => 'berita'));
        }
            $this->Session->setFlash(
                __('Berita gagal dihapus'), "flash_error"
            );
    }
    
    public function guru() {
        $this->layout = "dashboard";
        $this->modelClass = "Guru";
        $this->setCurrentPage("Data Guru");
        $this->paginate = array(
            'limit' => 10,
            'recursive' => 0,
            'order' => array('nama' => 'ASC')
        );
        $this->set('guru', $this->paginate('Guru'));
        if ($this->RequestHandler->isAjax()) {
            $this->autoRender = FALSE;
            $this->layout = NULL;
            if ($this->RequestHandler->isAjax()) {
                $this->render('ajax_guru', 'ajax');
            }
        }
    }
    
    public function add_guru() {
        $this->modelClass = "Guru";
        if ($this->request->is('post')) {
            $this->Guru->create();
            if ($this->Guru->save($this->request->data)) {
                $this->Session->setFlash("Data Guru Berhasil Di Tambah", "flash_success");
                return $this->redirect(array('action' => 'guru'));
            }
            $this->Session->setFlash(__("Data Guru Gagal Di Tambah"), "flash_error");
        }
    }
    
    public function edit_guru() {
        $this->modelClass = "Guru";
        $id = $this->request->data['Guru']['id'];
        
        $guru = $this->Guru->findById($id);
        if (!$guru) {
            throw new NotFoundException(__('Data Guru tidak ditemukan'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $this->Guru->Id = $id;
            if($this->Guru->save($this->request->data)) {
                $this->Session->setFlash("Data Guru Berhasil Di Edit", "flash_success");
                return $this->redirect(array('action' => 'guru'));
            }
            $this->Session->setFlash("Data Guru Gagal Di Edit", "flash_error");
        }
    }
    
    public function delete_guru($id) {
        $this->modelClass = "Guru";
        if ($this->Guru->delete($id)) {
            $this->Session->setFlash(
                __('Data Guru telah berhasil dihapus'), "flash_success"
            );
            return $this->redirect(array('action' => 'guru'));
        }
            $this->Session->setFlash(
                __('Data Guru gagal dihapus'), "flash_error"
            );
    }
    
    public function siswa() {
        $this->layout = "dashboard";
        $this->loadModel('Siswa');
        $this->loadModel('Jurusan');
        $this->setCurrentPage("Data Siswa/i");
        $this->paginate = array(
            'limit' => 10,
            'recursive' => 0,
            'order' => array('nama' => 'ASC')
        );
        $this->set('siswa', $this->paginate('Siswa'));
        $jurusan = $this->Jurusan->find('list', array("fields" => array("id_jurusan", "nama_jurusan")));
        $this->set('jurusan', $jurusan);
        if ($this->RequestHandler->isAjax()) {
            $this->autoRender = FALSE;
            $this->layout = NULL;
            if ($this->RequestHandler->isAjax()) {
                $this->render('ajax_siswa', 'ajax');
            }
        }
    }
    
    public function add_siswa() {
        $this->loadModel("Siswa");
        if ($this->request->is('post')) {
            $this->Siswa->create();
            if ($this->Siswa->save($this->request->data)) {
                $this->Session->setFlash("Data Siswa/i Berhasil Di Tambah", "flash_success");
                return $this->redirect(array('action' => 'siswa'));
            }
            $this->Session->setFlash(__("Data Siswa/i Gagal Di Tambah"), "flash_error");
        }
    }
    
    public function edit_siswa() {
        $this->modelClass = "Siswa";
        $id = $this->request->data['Siswa']['id'];
        
        $siswa = $this->Siswa->findById($id);
        if (!$siswa) {
            throw new NotFoundException(__('Data Siswa tidak ditemukan'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $this->Siswa->id = $id;
            if($this->Siswa->save($this->request->data)) {
                $this->Session->setFlash("Data Siswa Berhasil Di Edit", "flash_success");
                return $this->redirect(array('action' => 'siswa'));
            }
            $this->Session->setFlash("Data Siswa Gagal Di Edit", "flash_error");
        }
    }
    
    public function delete_siswa($id) {
        $this->modelClass = "Siswa";
        if ($this->Siswa->delete($id)) {
            $this->Session->setFlash(
                __('Data Siswa/i telah berhasil dihapus'), "flash_success"
            );
            return $this->redirect(array('action' => 'siswa'));
        }
            $this->Session->setFlash(
                __('Data Siswa/i gagal dihapus'), "flash_error"
            );
    }
}
