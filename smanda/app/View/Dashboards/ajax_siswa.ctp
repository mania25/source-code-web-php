<?php
    $this->Paginator->options(array(
        'update' => '.siswa',
        'evalScripts' => TRUE,
        'url' => array('controller' => 'Dashboards', 'action' => 'siswa'),
        'before' => $this->Js->get('.siswa')->effect('fadeOut'),
        'complete' => $this->Js->get('.siswa')->effect('fadeIn')
    ));
?>
<table class="table table-hover table-responsive">
    <thead>
        <tr>
            <th>NIS</th>
            <th>Nama</th>
            <th>Tempat Lahir</th>
            <th>Tanggal Lahir</th>
            <th>Alamat</th>
            <th>Jenis Kelamin</th>
            <th>Jurusan</th>
            <th>Kelas</th>
            <th>Tahun Masuk</th>
        </tr>
    </thead>
    <tbody>
        <?php if (sizeof($siswa) != 0) { ?>
            <?php foreach ($siswa as $siswa) : ?>
                <tr>
                    <td style="display: none"><?php echo $siswa['Siswa']['id']; ?></td>
                    <td><?php echo $siswa['Siswa']['nis']; ?></td>
                    <td><?php echo $siswa['Siswa']['nama']; ?></td>
                    <td><?php echo $siswa['Siswa']['tmpt_lahir']; ?></td>
                    <td><?php echo $siswa['Siswa']['tgl_lahir']; ?></td>
                    <td><?php echo $siswa['Siswa']['alamat']; ?></td>
                    <td><?php echo $siswa['Siswa']['jenis_kelamin']; ?></td>
                    <td><?php echo $siswa['Jurusan']['nama_jurusan']; ?></td>
                    <td><?php echo $siswa['Kelas']['nama_kelas']; ?></td>
                    <td><?php echo $siswa['Siswa']['thn_masuk']; ?></td>
                    <td>
                        <?php echo $this->Html->link('<i class="fa fa-file-o"></i> View', '#lihatData', array("escape" => FALSE, "class" => "btn btn-md btn-info view-siswa", "data-toggle" => "modal")) ?>
                        <?php echo $this->Html->link('<i class="fa fa-edit"></i> Edit', '#editData', array("escape" => FALSE, "class" => "btn btn-md btn-warning edit-siswa", "data-toggle" => "modal")) ?>
                        <?php echo $this->Form->postLink('<i class="fa fa-trash"></i> Delete', array("action" => "delete_siswa", $siswa['Siswa']['id']), array("escape" => FALSE, "class" => "btn btn-md btn-danger", "confirm" => "Apakah anda yakin ?")) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php } else { ?>
                <tr>
                    <td colspan="8"><center> Data Siswa tidak tersedia </center></td>
                </tr>
        <?php } ?>
    </tbody>
</table>
<ul class="pagination">
    <?php
      echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
      echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
      echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
    ?>
</ul>