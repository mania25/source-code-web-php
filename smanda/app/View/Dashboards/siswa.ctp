<?php
    $this->Paginator->options(array(
        'update' => '.siswa',
        'evalScripts' => TRUE,
        'url' => array('controller' => 'Dashboards', 'action' => 'siswa'),
        'before' => $this->Js->get('.siswa')->effect('fadeOut'),
        'complete' => $this->Js->get('.siswa')->effect('fadeIn')
    ));
?>
<?php echo $this->Html->link('<i class="fa fa-plus"></i> Tambah Data Siswa/i', "#tambahData", array("class" => "btn btn-md btn-primary tambah-siswa", "escape" => false, "data-toggle" => "modal")) ?>
<br/><br/>
<div class="siswa">
    <table class="table table-hover table-responsive">
        <thead>
            <tr>
                <th>NIS</th>
                <th>Nama</th>
                <th>Tempat Lahir</th>
                <th>Tanggal Lahir</th>
                <th>Alamat</th>
                <th>Jenis Kelamin</th>
                <th>Jurusan</th>
                <th>Kelas</th>
                <th>Tahun Masuk</th>
            </tr>
        </thead>
        <tbody>
            <?php if (sizeof($siswa) != 0) { ?>
                <?php foreach ($siswa as $siswa) : ?>
                    <tr>
                        <td style="display: none"><?php echo $siswa['Siswa']['id']; ?></td>
                        <td><?php echo $siswa['Siswa']['nis']; ?></td>
                        <td><?php echo $siswa['Siswa']['nama']; ?></td>
                        <td><?php echo $siswa['Siswa']['tmpt_lahir']; ?></td>
                        <td><?php echo $siswa['Siswa']['tgl_lahir']; ?></td>
                        <td><?php echo $siswa['Siswa']['alamat']; ?></td>
                        <td><?php echo $siswa['Siswa']['jenis_kelamin']; ?></td>
                        <td><?php echo $siswa['Jurusan']['nama_jurusan']; ?></td>
                        <td><?php echo $siswa['Kelas']['nama_kelas']; ?></td>
                        <td><?php echo $siswa['Siswa']['thn_masuk']; ?></td>
                        <td>
                            <?php echo $this->Html->link('<i class="fa fa-file-o"></i> View', '#lihatData', array("escape" => FALSE, "class" => "btn btn-md btn-info view-siswa", "data-toggle" => "modal")) ?>
                            <?php echo $this->Html->link('<i class="fa fa-edit"></i> Edit', '#editData', array("escape" => FALSE, "class" => "btn btn-md btn-warning edit-siswa", "data-toggle" => "modal")) ?>
                            <?php echo $this->Form->postLink('<i class="fa fa-trash"></i> Delete', array("action" => "delete_siswa", $siswa['Siswa']['id']), array("escape" => FALSE, "class" => "btn btn-md btn-danger", "confirm" => "Apakah anda yakin ?")) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php } else { ?>
                    <tr>
                        <td colspan="8"><center> Data Siswa tidak tersedia </center></td>
                    </tr>
            <?php } ?>
        </tbody>
    </table>
    <ul class="pagination">
    <?php
      echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
      echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
      echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
    ?>
    </ul>
</div>
<div id="tambahData" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h3 id="myModalLabel">Tambah Data Siswa/i</h3>
                </div>
                <div class="modal-body">
                <?php echo $this->Form->create("Siswa", array('url' => array('controller' => 'Dashboards', 'action' => 'add_siswa'), 'inputDefaults' => array('label' => false,'div' => false))); ?>
                    <?php echo $this->Form->input('nis', array("label" => "NIS", "class" => "form-control", "div" => FALSE)) ?><br/>
                    <?php echo $this->Form->input('nama', array("label" => "Nama", "class" => "form-control", "div" => FALSE)) ?><br/>
                    <?php echo $this->Form->input('tmpt_lahir', array("label" => "Kota Kelahiran", "class" => "form-control", "div" => FALSE)) ?><br/>
                    <?php echo $this->Form->input('tgl_lahir', array("id" => "tanggal_lahir","type" => "text", "label" => "Tanggal Lahir", "class" => "form-control", "div" => FALSE)) ?><br/>
                    <?php echo $this->Form->input('jenis_kelamin', array("options" => array("Laki-Laki" => "Laki-Laki", "Perempuan" => "Perempuan"), "class" => "form-control", "label" => "Jenis Kelamin", "div" => FALSE, "empty" => "-- Jenis Kelamin --")) ?><br/>
                    <?php echo $this->Form->input('id_jurusan', array("options" => $jurusan, "class" => "form-control", "label" => "Jurusan", "div" => FALSE, "empty" => "-- Jurusan --")) ?><br/>
                    <?php echo $this->Form->input('kelas', array("options" => array(""), "class" => "form-control", "label" => "Kelas", "div" => FALSE, "empty" => "-- Kelas --")) ?><br/>
                    <?php echo $this->Form->input('alamat', array("label" => "Alamat", "class" => "form-control", "div" => FALSE)) ?><br/>
                    <?php $years = range(1954, date('Y') + 1); $years = array_combine($years, $years); echo $this->Form->input('thn_masuk', array("label" => "Tahun Masuk", "class" => "form-control", "options" => $years, "empty" => "-- Tahun Masuk --"))?><br/>
                </div>
                <div class="modal-footer">
                        <?php echo $this->Form->button('Batal', array("class" => "btn", "data-dismiss" => "modal", "aria-hidden" => "TRUE")) ?>
                        <?php echo $this->Form->submit('Tambah Data', array("class" => "btn btn-primary", "div" => FALSE)) ?>
                </div>
                <?php echo $this->Form->end() ?>
        </div>
    </div>
</div>
<div id="lihatData" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h3 id="myModalLabel">Detail Siswa/i</h3>
                </div>
                <div class="modal-body">
                <?php echo $this->Form->create("Siswa", array('url' => array('controller' => 'Dashboards', 'action' => 'add_siswa'), 'inputDefaults' => array('label' => false,'div' => false))); ?>
                    <?php echo $this->Form->input('nis', array("id" => "nis" ,"label" => "NIS", "class" => "form-control", "div" => FALSE, "disabled")) ?><br/>
                    <?php echo $this->Form->input('nama', array("id" => "nama" ,"label" => "Nama", "class" => "form-control", "div" => FALSE, "disabled")) ?><br/>
                    <?php echo $this->Form->input('tmpt_lahir', array("id" => "kota_lahir" ,"label" => "Kota Kelahiran", "class" => "form-control", "div" => FALSE, "disabled")) ?><br/>
                    <?php echo $this->Form->input('tgl_lahir', array("id" => "tanggal_lahir" ,"type" => "text", "label" => "Tanggal Lahir", "class" => "form-control", "div" => FALSE, "disabled")) ?><br/>
                    <?php echo $this->Form->input('jenis_kelamin', array("id" => "jk" ,"options" => array("Laki-Laki" => "Laki-Laki", "Perempuan" => "Perempuan"), "class" => "form-control", "label" => "Jenis Kelamin", "div" => FALSE, "empty" => "-- Jenis Kelamin --", "disabled")) ?><br/>
                    <?php echo $this->Form->input('id_jurusan', array("id" => "jurusan" ,"options" => $jurusan, "class" => "form-control", "label" => "Jurusan", "div" => FALSE, "empty" => "-- Jurusan --", "disabled")) ?><br/>
                    <?php echo $this->Form->input('kelas', array("id" => "kelas" ,"options" => array(""), "class" => "form-control", "label" => "Kelas", "div" => FALSE, "empty" => "-- Kelas --", "disabled")) ?><br/>
                    <?php echo $this->Form->input('alamat', array("id" => "alamat" ,"label" => "Alamat", "class" => "form-control", "div" => FALSE, "disabled")) ?><br/>
                    <?php $years = range(1954, date('Y') + 1); $years = array_combine($years, $years); echo $this->Form->input('thn_masuk', array("id" => "tahun_masuk" ,"label" => "Tahun Masuk", "class" => "form-control", "options" => $years, "empty" => "-- Tahun Masuk --", "disabled"))?><br/>
                </div>
                <div class="modal-footer">
                        <?php echo $this->Form->button('Batal', array("class" => "btn", "data-dismiss" => "modal", "aria-hidden" => "TRUE")) ?>
                </div>
                <?php echo $this->Form->end() ?>
        </div>
    </div>
</div>
<div id="editData" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h3 id="myModalLabel">Edit Siswa/i</h3>
                </div>
                <div class="modal-body">
                <?php echo $this->Form->create("Siswa", array('url' => array('controller' => 'Dashboards', 'action' => 'edit_siswa'), 'inputDefaults' => array('label' => false,'div' => false))); ?>
                    <?php echo $this->Form->input('id', array("label" => false, "div" => FALSE, "type" => "hidden", "id" => "id")) ?>
                    <?php echo $this->Form->input('nis', array("id" => "nis" ,"label" => "NIS", "class" => "form-control", "div" => FALSE)) ?><br/>
                    <?php echo $this->Form->input('nama', array("id" => "nama" ,"label" => "Nama", "class" => "form-control", "div" => FALSE)) ?><br/>
                    <?php echo $this->Form->input('tmpt_lahir', array("id" => "kota_lahir" ,"label" => "Kota Kelahiran", "class" => "form-control", "div" => FALSE)) ?><br/>
                    <?php echo $this->Form->input('tgl_lahir', array("id" => "tanggal_lahir" ,"type" => "text", "label" => "Tanggal Lahir", "class" => "form-control", "div" => FALSE)) ?><br/>
                    <?php echo $this->Form->input('jenis_kelamin', array("id" => "jk" ,"options" => array("Laki-Laki" => "Laki-Laki", "Perempuan" => "Perempuan"), "class" => "form-control", "label" => "Jenis Kelamin", "div" => FALSE, "empty" => "-- Jenis Kelamin --")) ?><br/>
                    <?php echo $this->Form->input('id_jurusan', array("id" => "jurusan" ,"options" => $jurusan, "class" => "form-control", "label" => "Jurusan", "div" => FALSE, "empty" => "-- Jurusan --")) ?><br/>
                    <?php echo $this->Form->input('kelas', array("id" => "kelas" ,"options" => array(""), "class" => "form-control", "label" => "Kelas", "div" => FALSE, "empty" => "-- Kelas --")) ?><br/>
                    <?php echo $this->Form->input('alamat', array("id" => "alamat" ,"label" => "Alamat", "class" => "form-control", "div" => FALSE)) ?><br/>
                    <?php $years = range(1954, date('Y') + 1); $years = array_combine($years, $years); echo $this->Form->input('thn_masuk', array("id" => "tahun_masuk" ,"label" => "Tahun Masuk", "class" => "form-control", "options" => $years, "empty" => "-- Tahun Masuk --"))?><br/>
                </div>
                <div class="modal-footer">
                        <?php echo $this->Form->button('Batal', array("class" => "btn", "data-dismiss" => "modal", "aria-hidden" => "TRUE")) ?>
                        <?php echo $this->Form->submit('Edit Data', array("class" => "btn btn-primary", "div" => FALSE)) ?>
                </div>
                <?php echo $this->Form->end() ?>
        </div>
    </div>
</div>