<?php
    $this->Paginator->options(array(
        'update' => '.berita',
        'evalScripts' => TRUE,
        'url' => array('controller' => 'Dashboards', 'action' => 'berita'),
        'before' => $this->Js->get('.berita')->effect('fadeOut'),
        'complete' => $this->Js->get('.berita')->effect('fadeIn')
    ));
?>
<?php echo $this->Html->link('<i class="fa fa-plus"></i> Tambah Berita', "#tambahData", array("class" => "btn btn-md btn-primary", "escape" => false, "data-toggle" => "modal")) ?>
<br/><br/>
<div class="berita">
    <table class="table table-hover table-responsive">
        <thead>
            <tr>
                <th>Judul</th>
                <th>Isi</th>
                <th>Waktu Posting</th>
                <th>Penulis</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php if (sizeof($berita) != 0) { ?>
                <?php foreach ($berita as $berita) : ?>
                    <tr>
                        <td style="display: none"><?php echo $berita['Berita']['id']; ?></td>
                        <td><?php echo $berita['Berita']['judul']; ?></td>
                        <td><?php echo substr($berita['Berita']['isi'], 0, 20).'...'; ?></td>
                        <td><?php echo $berita['Berita']['tgl_agenda']; ?></td>
                        <td><?php echo $berita['User']['nama']; ?></td>
                        <td>
                            <?php echo $this->Html->link('<i class="fa fa-file-o"></i> View', '#lihatData', array("escape" => FALSE, "class" => "btn btn-md btn-info view", "data-toggle" => "modal")) ?>
                            <?php echo $this->Html->link('<i class="fa fa-edit"></i> Edit', '#editData', array("escape" => FALSE, "class" => "btn btn-md btn-warning edit", "data-toggle" => "modal")) ?>
                            <?php echo $this->Form->postLink('<i class="fa fa-trash"></i> Delete', array("action" => "delete_berita", $berita['Berita']['id']), array("escape" => FALSE, "class" => "btn btn-md btn-danger", "confirm" => "Apakah anda yakin ?")) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php } else { ?>
                    <tr>
                        <td colspan="5"><center> Berita tidak tersedia </center></td>
                    </tr>
            <?php } ?>
        </tbody>
    </table>
    <ul class="pagination">
    <?php
      echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
      echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
      echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
    ?>
    </ul>
</div>
<div id="tambahData" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h3 id="myModalLabel">Tambah Berita</h3>
                </div>
                <?php echo $this->Form->create("Berita", array('url' => array('controller' => 'Dashboards', 'action' => 'add_berita'), 'inputDefaults' => array('label' => false,'div' => false))); ?>
                <div class="modal-body">
                    <?php echo $this->Form->input('judul', array("label" => "Judul", "class" => "form-control", "div" => FALSE)) ?><br/>
                    <?php echo $this->Form->input('isi', array("label" => "Isi Berita", "class" => "ckeditor", "div" => FALSE)) ?>
                </div>
                <div class="modal-footer">
                        <?php echo $this->Form->button('Batal', array("class" => "btn", "data-dismiss" => "modal", "aria-hidden" => "TRUE")) ?>
                        <?php echo $this->Form->submit('Tambah Data', array("class" => "btn btn-primary", "div" => FALSE)) ?>
                </div>
                <?php echo $this->Form->end() ?>
        </div>
    </div>
</div>
<div id="lihatData" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h3 id="myModalLabel">Detail Berita</h3>
                </div>
                <div class="modal-body">
                    <?php echo $this->Form->create('Berita', array('url' => '', 'inputDefaults' => array('label' => false,'div' => false))); ?>
                    <?php echo $this->Form->input('judul', array("label" => "Judul", "class" => "form-control", "id" => "judul", "div" => FALSE, "disabled" => true)) ?><br/>
                    <?php echo $this->Form->input('isi', array("label" => "Isi Berita", "class" => "form-control", "id" => "isi", "div" => FALSE, "disabled" => true)) ?>
                </div>
                <div class="modal-footer">
                        <?php echo $this->Form->button('Close', array("class" => "btn", "data-dismiss" => "modal", "aria-hidden" => "TRUE")) ?>
                </div>
                <?php echo $this->Form->end() ?>
        </div>
    </div>
</div>
<div id="editData" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h3 id="myModalLabel">Edit Berita</h3>
                </div>
                <div class="modal-body">
                    <?php echo $this->Form->create('Berita', array('url' => array('controller' => 'Dashboards', 'action' => 'edit_berita'), 'inputDefaults' => array('label' => false,'div' => false))); ?>
                    <?php echo $this->Form->hidden('id', array("label" => FALSE, "div" => FALSE, "id" => "id")) ?>
                    <?php echo $this->Form->input('judul', array("label" => "Judul", "class" => "form-control", "div" => FALSE, "id" => "judul")) ?><br/>
                    <?php echo $this->Form->input('isi', array("label" => "Isi Berita", "class" => "form-control ckeditor", "div" => FALSE, "id" => "isi")) ?>
                </div>
                <div class="modal-footer">
                        <?php echo $this->Form->button('Close', array("class" => "btn", "data-dismiss" => "modal", "aria-hidden" => "TRUE")) ?>
                        <?php echo $this->Form->submit('Edit Data', array("class" => "btn btn-primary", "div" => FALSE)) ?>                
                        <?php echo $this->Form->end() ?>
                </div>
        </div>
    </div>
</div>