<?php
    $this->Paginator->options(array(
        'update' => '.berita',
        'evalScripts' => TRUE,
        'url' => array('controller' => 'Dashboards', 'action' => 'berita'),
        'before' => $this->Js->get('.berita')->effect('fadeOut'),
        'complete' => $this->Js->get('.berita')->effect('fadeIn')
    ));
?>
<table class="table table-hover table-responsive">
        <thead>
            <tr>
                <th>Judul</th>
                <th>Isi</th>
                <th>Waktu Posting</th>
                <th>Penulis</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php if (sizeof($berita) != 0) { ?>
                <?php foreach ($berita as $berita) : ?>
                    <tr>
                        <td style="display: none"><?php echo $berita['Berita']['id']; ?></td>
                        <td><?php echo $berita['Berita']['judul']; ?></td>
                        <td><?php echo substr($berita['Berita']['isi'], 0, 20).'...'; ?></td>
                        <td><?php echo $berita['Berita']['tgl_agenda']; ?></td>
                        <td><?php echo $berita['User']['nama']; ?></td>
                        <td>
                            <?php echo $this->Html->link('<i class="fa fa-file-o"></i> View', '#lihatData', array("escape" => FALSE, "class" => "btn btn-md btn-info view", "data-toggle" => "modal")) ?>
                            <?php echo $this->Html->link('<i class="fa fa-edit"></i> Edit', '#editData', array("escape" => FALSE, "class" => "btn btn-md btn-warning edit", "data-toggle" => "modal")) ?>
                            <?php echo $this->Form->postLink('<i class="fa fa-trash"></i> Delete', array("action" => "delete_berita", $berita['Berita']['id']), array("escape" => FALSE, "class" => "btn btn-md btn-danger", "confirm" => "Apakah anda yakin ?")) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php } else { ?>
                    <tr>
                        <td colspan="5"><center> Berita tidak tersedia </center></td>
                    </tr>
            <?php } ?>
        </tbody>
    </table>
    <ul class="pagination">
    <?php
      echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
      echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
      echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
    ?>
    </ul>