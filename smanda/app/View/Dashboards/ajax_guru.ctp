<?php
    $this->Paginator->options(array(
        'update' => '.guru',
        'evalScripts' => TRUE,
        'url' => array('controller' => 'Dashboards', 'action' => 'guru'),
        'before' => $this->Js->get('.guru')->effect('fadeOut'),
        'complete' => $this->Js->get('.guru')->effect('fadeIn')
    ));
?>
    <table class="table table-hover table-responsive">
        <thead>
            <tr>
                <th>NIP</th>
                <th>Nama</th>
                <th>Tempat Lahir</th>
                <th>Tanggal Lahir</th>
                <th>Jabatan</th>
                <th>Pendidikan Terakhir</th>
                <th>Mata Pelajaran</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php if (sizeof($guru) != 0) { ?>
                <?php foreach ($guru as $guru) : ?>
                    <tr>
                        <td style="display: none"><?php echo $guru['Guru']['id']; ?></td>
                        <td><?php echo $guru['Guru']['nip']; ?></td>
                        <td><?php echo $guru['Guru']['nama']; ?></td>
                        <td><?php echo $guru['Guru']['tmpt_lahir']; ?></td>
                        <td><?php echo $guru['Guru']['tgl_lahir']; ?></td>
                        <td><?php echo $guru['Guru']['jabatan']; ?></td>
                        <td><?php echo $guru['Guru']['pend_terakhir']; ?></td>
                        <td><?php echo $guru['Guru']['pelajaran']; ?></td>
                        <td>
                            <?php echo $this->Html->link('<i class="fa fa-file-o"></i> View', '#lihatData', array("escape" => FALSE, "class" => "btn btn-md btn-info view-guru", "data-toggle" => "modal")) ?>
                            <?php echo $this->Html->link('<i class="fa fa-edit"></i> Edit', '#editData', array("escape" => FALSE, "class" => "btn btn-md btn-warning edit-guru", "data-toggle" => "modal")) ?>
                            <?php echo $this->Form->postLink('<i class="fa fa-trash"></i> Delete', array("action" => "delete_guru", $guru['Guru']['id']), array("escape" => FALSE, "class" => "btn btn-md btn-danger", "confirm" => "Apakah anda yakin ?")) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php } else { ?>
                    <tr>
                        <td colspan="8"><center> Data Guru tidak tersedia </center></td>
                    </tr>
            <?php } ?>
        </tbody>
    </table>
    <ul class="pagination">
    <?php
      echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
      echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
      echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
    ?>