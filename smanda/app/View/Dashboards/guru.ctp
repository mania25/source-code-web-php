<?php
    $this->Paginator->options(array(
        'update' => '.guru',
        'evalScripts' => TRUE,
        'url' => array('controller' => 'Dashboards', 'action' => 'guru'),
        'before' => $this->Js->get('.guru')->effect('fadeOut'),
        'complete' => $this->Js->get('.guru')->effect('fadeIn')
    ));
?>
<?php echo $this->Html->link('<i class="fa fa-plus"></i> Tambah Data Guru', "#tambahData", array("class" => "btn btn-md btn-primary tambah-guru", "escape" => false, "data-toggle" => "modal")) ?>
<br/><br/>
<div class="guru">
    <table class="table table-hover table-responsive">
        <thead>
            <tr>
                <th>NIP</th>
                <th>Nama</th>
                <th>Tempat Lahir</th>
                <th>Tanggal Lahir</th>
                <th>Jabatan</th>
                <th>Pendidikan Terakhir</th>
                <th>Mata Pelajaran</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php if (sizeof($guru) != 0) { ?>
                <?php foreach ($guru as $guru) : ?>
                    <tr>
                        <td style="display: none"><?php echo $guru['Guru']['id']; ?></td>
                        <td><?php echo $guru['Guru']['nip']; ?></td>
                        <td><?php echo $guru['Guru']['nama']; ?></td>
                        <td><?php echo $guru['Guru']['tmpt_lahir']; ?></td>
                        <td><?php echo $guru['Guru']['tgl_lahir']; ?></td>
                        <td><?php echo $guru['Guru']['jabatan']; ?></td>
                        <td><?php echo $guru['Guru']['pend_terakhir']; ?></td>
                        <td><?php echo $guru['Guru']['pelajaran']; ?></td>
                        <td>
                            <?php echo $this->Html->link('<i class="fa fa-file-o"></i> View', '#lihatData', array("escape" => FALSE, "class" => "btn btn-md btn-info view-guru", "data-toggle" => "modal")) ?>
                            <?php echo $this->Html->link('<i class="fa fa-edit"></i> Edit', '#editData', array("escape" => FALSE, "class" => "btn btn-md btn-warning edit-guru", "data-toggle" => "modal")) ?>
                            <?php echo $this->Form->postLink('<i class="fa fa-trash"></i> Delete', array("action" => "delete_guru", $guru['Guru']['id']), array("escape" => FALSE, "class" => "btn btn-md btn-danger", "confirm" => "Apakah anda yakin ?")) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php } else { ?>
                    <tr>
                        <td colspan="8"><center> Data Guru tidak tersedia </center></td>
                    </tr>
            <?php } ?>
        </tbody>
    </table>
    <ul class="pagination">
    <?php
      echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
      echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
      echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
    ?>
    </ul>
</div>
<div id="tambahData" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h3 id="myModalLabel">Tambah Data Guru</h3>
                </div>
                <div class="modal-body">
                <?php echo $this->Form->create("Guru", array('url' => array('controller' => 'Dashboards', 'action' => 'add_guru'), 'inputDefaults' => array('label' => false,'div' => false))); ?>
                    <?php echo $this->Form->input('nip', array("label" => "NIP", "class" => "form-control", "div" => FALSE)) ?><br/>
                    <?php echo $this->Form->input('nama', array("label" => "Nama", "class" => "form-control", "div" => FALSE)) ?><br/>
                    <?php echo $this->Form->input('tmpt_lahir', array("label" => "Kota Kelahiran", "class" => "form-control", "div" => FALSE)) ?><br/>
                    <?php echo $this->Form->input('tgl_lahir', array("type" => "text", "id" => "ttl", "label" => "Tanggal Lahir", "class" => "form-control", "div" => FALSE)) ?><br/>
                    <?php echo $this->Form->input('pelajaran', array("label" => "Mata Pelajaran", "class" => "form-control", "div" => FALSE)) ?><br/>
                    <?php echo $this->Form->input('pend_terakhir', array("options" => array("SD" => "SD", "SMP" => "SMP", "SMA" => "SMA", "D3" => "D3", "S1" => "S1", "S2" => "S2", "S3" => "S3"), "empty" => "-- Pendidikan Terakhir --","label" => "Pendidikan Terakhir", "class" => "form-control", "div" => FALSE)) ?><br/>
                    <?php echo $this->Form->input('jabatan', array("options" => array("Guru" => "Guru"), "empty" => "-- Jabatan --","label" => "Jabatan", "class" => "form-control", "div" => FALSE)) ?><br/>
                </div>
                <div class="modal-footer">
                        <?php echo $this->Form->button('Batal', array("class" => "btn", "data-dismiss" => "modal", "aria-hidden" => "TRUE")) ?>
                        <?php echo $this->Form->submit('Tambah Data', array("class" => "btn btn-primary", "div" => FALSE)) ?>
                </div>
                <?php echo $this->Form->end() ?>
        </div>
    </div>
</div>
<div id="lihatData" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h3 id="myModalLabel">Detail Guru</h3>
                </div>
                <div class="modal-body">
                <?php echo $this->Form->create("Guru", array('url' => '#', 'inputDefaults' => array('label' => false,'div' => false))); ?>
                    <?php echo $this->Form->input('nip', array("label" => "NIP", "class" => "form-control", "div" => FALSE, "disabled", "id" => "nip")) ?><br/>
                    <?php echo $this->Form->input('nama', array("label" => "Nama", "class" => "form-control", "div" => FALSE, "disabled", "id" => "nama")) ?><br/>
                    <?php echo $this->Form->input('tmpt_lahir', array("label" => "Kota Kelahiran", "class" => "form-control", "div" => FALSE, "disabled", "id" => "kota_lahir")) ?><br/>
                    <?php echo $this->Form->input('tgl_lahir', array("type" => "text", "id" => "ttl", "label" => "Tanggal Lahir", "class" => "form-control", "div" => FALSE, "disabled")) ?><br/>
                    <?php echo $this->Form->input('pelajaran', array("label" => "Mata Pelajaran", "class" => "form-control", "div" => FALSE, "disabled", "id" => "mapel")) ?><br/>
                    <?php echo $this->Form->input('pend_terakhir', array("id" => "pendidikan_terakhir", "options" => array("SD" => "SD", "SMP" => "SMP", "SMA" => "SMA", "D3" => "D3", "S1" => "S1", "S2" => "S2", "S3" => "S3"), "empty" => "-- Pendidikan Terakhir --","label" => "Pendidikan Terakhir", "class" => "form-control", "div" => FALSE, "disabled")) ?><br/>
                    <?php echo $this->Form->input('jabatan', array("id" => "jabatan", "options" => array( "Guru" => "Guru"), "empty" => "-- Jabatan --","label" => "Jabatan", "class" => "form-control", "div" => FALSE, "disabled")) ?><br/>
                </div>
                <div class="modal-footer">
                        <?php echo $this->Form->button('Batal', array("class" => "btn", "data-dismiss" => "modal", "aria-hidden" => "TRUE")) ?>
                </div>
                <?php echo $this->Form->end() ?>
        </div>
    </div>
</div>
<div id="editData" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h3 id="myModalLabel">Edit Guru</h3>
                </div>
                <div class="modal-body">
                <?php echo $this->Form->create("Guru", array('url' => array("controller" => "Dashboards","action" => "edit_guru"), 'inputDefaults' => array('label' => false,'div' => false))); ?>
                    <?php echo $this->Form->input('id', array("label" => false, "div" => FALSE, "type" => "hidden", "id" => "id")) ?>
                    <?php echo $this->Form->input('nip', array("label" => "NIP", "class" => "form-control", "div" => FALSE, "id" => "nip")) ?><br/>
                    <?php echo $this->Form->input('nama', array("label" => "Nama", "class" => "form-control", "div" => FALSE, "id" => "nama")) ?><br/>
                    <?php echo $this->Form->input('tmpt_lahir', array("label" => "Kota Kelahiran", "class" => "form-control", "div" => FALSE, "id" => "kota_lahir")) ?><br/>
                    <?php echo $this->Form->input('tgl_lahir', array("type" => "text", "id" => "ttl", "label" => "Tanggal Lahir", "class" => "form-control", "div" => FALSE)) ?><br/>
                    <?php echo $this->Form->input('pelajaran', array("label" => "Mata Pelajaran", "class" => "form-control", "div" => FALSE, "id" => "mapel")) ?><br/>
                    <?php echo $this->Form->input('pend_terakhir', array("id" => "pendidikan_terakhir", "options" => array("SD" => "SD", "SMP" => "SMP", "SMA" => "SMA", "D3" => "D3", "S1" => "S1", "S2" => "S2", "S3" => "S3"), "empty" => "-- Pendidikan Terakhir --","label" => "Pendidikan Terakhir", "class" => "form-control", "div" => FALSE)) ?><br/>
                    <?php echo $this->Form->input('jabatan', array("id" => "jabatan", "options" => array( "Guru" => "Guru"), "empty" => "-- Jabatan --","label" => "Jabatan", "class" => "form-control", "div" => FALSE)) ?><br/>
                </div>
                <div class="modal-footer">
                        <?php echo $this->Form->button('Batal', array("class" => "btn", "data-dismiss" => "modal", "aria-hidden" => "TRUE")) ?>
                        <?php echo $this->Form->submit('Edit Data', array("class" => "btn btn-primary", "div" => FALSE)) ?>
                </div>
                <?php echo $this->Form->end() ?>
        </div>
    </div>
</div>