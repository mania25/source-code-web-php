<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Administrator - Dashboards</title>
    <style>
        .table td, .table th {
            text-align: center;
        }
    </style>
    <!-- Bootstrap Core CSS -->
    <?php echo $this->Html->css('bootstrap.min'); ?>

    <!-- MetisMenu CSS -->
    <?php echo $this->Html->css('plugins/metisMenu/metisMenu.min'); ?>

    <!-- Timeline CSS -->
    <?php echo $this->Html->css('plugins/timeline'); ?>

    <!-- Custom CSS -->
    <?php echo $this->Html->css('sb-admin-2'); ?>

    <!-- Morris Charts CSS -->
    <?php echo $this->Html->css('plugins/morris'); ?>

    <!-- Custom Fonts -->
    <?php echo $this->Html->css('font-awesome.min'); ?>

    <?php echo $this->Html->script('ckeditor/ckeditor');?>
    
    <?php echo $this->Html->script('jquery') ?>
    
    <?php 
        echo $this->fetch('script');
        echo $scripts_for_layout;
        echo $this->Js->writeBuffer(); 
    ?>
    
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               <?php echo $this->Html->link('Dashboard', array("action" => "index"), array("class" => "navbar-brand")) ?>
               
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="login.html"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <?php echo $this->Html->link('<i class="fa fa-dashboard fa-fw"></i> Dashboard', array("action" => "index"), array('escape' => false)) ?>
                        </li>
                        <li>
                            <?php echo $this->Html->link('<i class="fa fa-newspaper-o"></i>&nbsp; Berita</a>', array("action" => "berita"), array('escape' => false)) ?>
                        </li>
                        <li>
                            <?php echo $this->Html->link('<i class="fa fa-sitemap fa-fw"></i> Master Data <span class="fa arrow"></span>', '#', array('escape' => false)) ?>
                            <ul class="nav nav-second-level">
                                <li>
                                    <?php echo $this->Html->link('Guru <span class="fa arrow"></span>', '#', array('escape' => false)) ?>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <?php echo $this->Html->link('Tampil Data', array("action" => "guru"), array('escape' => false)) ?>
                                        </li>
                                    </ul>
                                    <!-- /.nav-third-level -->
                                </li>
                                <li>
                                    <?php echo $this->Html->link('Siswa/i <span class="fa arrow"></span>', '#', array('escape' => false)) ?>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <?php echo $this->Html->link('Tampil Data', array("action" => "siswa"), array('escape' => false)) ?>
                                        </li>
                                    </ul>
                                    <!-- /.nav-third-level -->
                                </li>
                                <li>
                                    <?php echo $this->Html->link('Alumni <span class="fa arrow"></span>', '#', array('escape' => false)) ?>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <?php echo $this->Html->link('Tampil Data', array("action" => "alumni"), array('escape' => false)) ?>
                                        </li>
                                    </ul>
                                    <!-- /.nav-third-level -->
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $currentPage; ?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <?php echo $this->Session->flash() ?>
                <?php echo $this->fetch('content') ?>
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <?php echo $this->Html->script('jquery') ?>

    <!-- Bootstrap Core JavaScript -->
    <?php echo $this->Html->script('bootstrap.min') ?>

    <!-- Metis Menu Plugin JavaScript -->
    <?php echo $this->Html->script('plugins/metisMenu/metisMenu.min') ?>

    <!-- Custom Theme JavaScript -->
    <?php echo $this->Html->script('sb-admin-2') ?>
    
    <?php // echo $this->element('sql_dump'); ?>
</body>

</html>
