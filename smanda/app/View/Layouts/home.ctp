<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="EN" lang="EN" dir="ltr">
<head profile="http://gmpg.org/xfn/11">
<title>Official Website SMAN 2 Koto Kampar Hulu</title>
<?php echo $this->Html->charset('ISO-8859-1');  ?>
<meta http-equiv="imagetoolbar" content="no" />
<?php echo $this->Html->css('layout'); ?>
<?php echo $this->Html->css('font-awesome.min'); ?>
<!-- Homepage Specific Elements -->
<?php echo $this->Html->script('jquery-1.4.1.min'); ?>
<?php echo $this->Html->script('jquery-ui-1.7.2.custom.min'); ?>
<?php echo $this->Html->script('jquery.tabs.setup'); ?>
<?php echo $this->fetch('script');
      echo $scripts_for_layout;
      echo $this->Js->writeBuffer(array('inline' => TRUE)); 
?>
<!-- End Homepage Specific Elements -->
</head>
<body id="top">
    <?php $username = $this->Session->read('Auth.User.nama'); ?>
<div class="wrapper row1">
  <div id="header" class="clear">
    <div class="fl_left">
      <h1><a href="index.html">SMAN 2 Koto Kampar Hulu</a></h1>
      <p>Jl. Lingkar Sibiruang, Kec.Koto Kampar Hulu, Kab. Kampar</p>
    </div>
    <?php if (!isset($username)) { ?>
        <div class="fl_right">
          <ul>
            <li><a href="#">Contact Us</a></li>
            <li><a href="#">Student Login</a></li>
            <li class="last"><a href="#">Staff Login</a></li>
          </ul>
        </div>
    <?php } else { ?>
        <div class="fl_right">
          <ul>
            <li>Welcome <?php $username; ?></li>
          </ul>
        </div>
    <?php } ?>
  </div>
</div>
<!-- ####################################################################################################### -->
<div class="wrapper row2">
    <!-- ###### -->
    
	<nav>
	<ul>
            <li><?php echo $this->Html->link('Beranda', array("action" => "index")) ?></li>
		<li><?php echo $this->Html->tag('a', 'Profil'); ?>
			<ul>
				<li><a href="pages/selayang-pandang.html">Selayang Pandang Kepala sekolah</a></li>
				<li><a href="#">Visi Misi</a></li>
				<li><a href="#">Sejarah Singkat</a></li>
				<li><a href="#">Sarana dan Prasarana</a></li>
				<li><a href="#">Struktur Organisasi</a></li>
				<li><a href="#">Program Kerja</a></li>
				<li><a href="#">Kondisi Siswa</a></li>
				<li><a href="#">Komite Sekolah</a></li>
				<li><a href="#">Dena Lokasi</a></li>	
			</ul>
		</li>
		<li><?php echo $this->Html->tag('a', 'Majelis Guru'); ?>
			<ul>
				<li><?php echo $this->Html->link('Direktori Guru', array("action" => "directory_guru")) ?></li>
				<li><a href="#">Silabus dan Materi Ajar</a></li>
				<li><a href="#">Kalender Akademik</a></li>
			</ul>
		</li>
				<li><?php echo $this->Html->tag('a', 'Peserta Didik'); ?>
			<ul>
				<li><?php echo $this->Html->link('Direktori Siswa/i', array("action" => "directory_siswa")) ?></li>
				<li><a href="#">OSIS</a></li>
				<li><a href="#">Rohis</a></li>
				<li><a href="#">Ekstrakurikuler</a></li>
			</ul>
		</li>
				<li><?php echo $this->Html->tag('a', 'Alumni'); ?>
			<ul>
				<li><?php echo $this->Html->link('Direktori Alumni', array("action" => "directory_alumni")) ?></li>
				<li><a href="#">Info dan Link Alumni</a></li>
			</ul>
		</li>
		<li><a href="menu/ppdb.html">PPDB 2015</a></li>
	</ul>
	</nav>
    
    <!-- ###### -->
</div>
<!-- ####################################################################################################### -->
<div class="wrapper">
  <div id="featured_slide" class="clear">
    <!-- ###### -->
    <div class="overlay_left"></div>
    <div id="featured_content">
      <div class="featured_box" id="fc1"><img src="images/demo/slider/1.gif" alt="" />
        <div class="floater">
          <h2>Nullamlacus dui ipsum</h2>
          <p>Attincidunt vel nam a maurisus lacinia consectetus magnisl sed ac morbi. Inmaurisus habitur pretium eu et ac vest penatibus id lacus parturpis.</p>
          <p class="readmore"><a href="#">Continue Reading &raquo;</a></p>
        </div>
      </div>
      <div class="featured_box" id="fc2"><img src="images/demo/slider/2.gif" alt="" />
        <div class="floater">
          <h2>Aliquatjusto quisque nam</h2>
          <p>Attincidunt vel nam a maurisus lacinia consectetus magnisl sed ac morbi. Inmaurisus habitur pretium eu et ac vest penatibus id lacus parturpis.</p>
          <p class="readmore"><a href="#">Continue Reading &raquo;</a></p>
        </div>
      </div>
      <div class="featured_box" id="fc3"><img src="images/demo/slider/3.gif" alt="" />
        <div class="floater">
          <h2>Aliquatjusto quisque nam</h2>
          <p>Attincidunt vel nam a maurisus lacinia consectetus magnisl sed ac morbi. Inmaurisus habitur pretium eu et ac vest penatibus id lacus parturpis.</p>
          <p class="readmore"><a href="#">Continue Reading &raquo;</a></p>
        </div>
      </div>
      <div class="featured_box" id="fc4"><img src="images/demo/slider/4.gif" alt="" />
        <div class="floater">
          <h2>Aliquatjusto quisque nam</h2>
          <p>Attincidunt vel nam a maurisus lacinia consectetus magnisl sed ac morbi. Inmaurisus habitur pretium eu et ac vest penatibus id lacus parturpis.</p>
          <p class="readmore"><a href="#">Continue Reading &raquo;</a></p>
        </div>
      </div>
      <div class="featured_box" id="fc5"><img src="images/demo/slider/5.gif" alt="" />
        <div class="floater">
          <h2>Dapiensociis temper donec</h2>
          <p>Attincidunt vel nam a maurisus lacinia consectetus magnisl sed ac morbi. Inmaurisus habitur pretium eu et ac vest penatibus id lacus parturpis.</p>
          <p class="readmore"><a href="#">Continue Reading &raquo;</a></p>
        </div>
      </div>
    </div>
    <ul id="featured_tabs">
      <li><a href="#fc1">All About The University</a></li>
      <li><a href="#fc2">Why You Should Study With Us</a></li>
      <li><a href="#fc3">Education And Student Experience</a></li>
      <li><a href="#fc4">Alumni And Its Donors</a></li>
      <li class="last"><a href="#fc5">Latest University News &amp; Events</a></li>
    </ul>
    <div class="overlay_right"></div>
    <!-- ###### -->
  </div>
</div>
<!-- ####################################################################################################### -->
<div class="wrapper row3">
  <div class="rnd">
      <div id="container" class="clear">
        <!-- ####################################################################################################### -->
        <div id="homepage" class="clear">
            <?php echo $this->Session->flash(); ?>
            <?php echo $this->fetch('content'); ?>
        </div>
      </div>
  </div>
</div>
<!-- ####################################################################################################### -->
<div class="wrapper row4">
  <div class="rnd">
    <div id="footer" class="clear">
      <!-- ####################################################################################################### -->
      <div class="fl_left clear">
        <address>
        Jl.Lingkar Sibiruang<br />
        Kecamatan Koto Kampar Hulu<br />
        Kabupaten Kampar<br />
        28453<br />
        <br />
        Tel: (0762) xxxxx<br />
        Email: <a href="#">smandaktkh@yahoo.com</a>
        </address>
      </div>
      <!-- ####################################################################################################### -->
    </div>
  </div>
</div>
<!-- ####################################################################################################### -->
<div class="wrapper">
  <div id="copyright" class="clear">
    <p class="fl_left">Copyright &copy; 2014 - All Rights Reserved - <a href="#">Official Smanda</a></p>
    <p class="fl_right">Template by <a href="http://www.os-templates.com/" title="Free Website Templates">OS Templates</a></p>
  </div>
</div>
</body>
</html>