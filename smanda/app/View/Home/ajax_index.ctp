<?php $paginator = $this->Paginator; ?>
<?php
    $this->Paginator->options(array(
        'update' => '.news',
        'evalScripts' => TRUE,
        'url' => array('controller' => 'Home', 'action' => 'index'),
        'before' => $this->Js->get('.news')->effect('fadeOut'),
        'complete' => $this->Js->get('.news')->effect('fadeIn')
    ));
?>
            <ul>
              <?php if(sizeof($berita) != 0) { ?>
                <?php foreach ($berita as $berita) : ?>
                      <li class="clear">
                        <div class="latestnews">
                            <p><?php echo $this->Html->link(ucfirst($berita['Berita']['judul']), array("action" => "detail_berita", $berita['Berita']['id'])) ?></p>
                            <p><?php echo substr($berita['Berita']['isi'], 0, 200) ?></p>
                        </div>
                      </li>
                <?php endforeach; ?>
                <p class="readmore">      
                  <?php echo $paginator->first(' First ', null, null, array('class' => 'disabled')); ?>
                  <?php echo $paginator->prev('Previous ', null, null, array('class' => 'disabled')); ?>
                  <?php echo $paginator->numbers(); ?>
                  <?php echo $paginator->next(' Next ', null, null, array('class' => 'disabled')); ?>
                  <?php echo $paginator->last(' Last ', null, null, array('class' => 'disabled')); ?>
                </p>
              <?php } else { ?>
                      <li class="clear">
                          <center><strong>Berita tidak tersedia.</strong></center>
                      </li>
              <?php } ?>
            </ul>