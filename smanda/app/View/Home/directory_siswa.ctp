<?php $paginator = $this->Paginator;?>
<?php
    $this->Paginator->options(array(
        'update' => '.data-siswa',
        'evalScripts' => TRUE,
        'url' => array('controller' => 'Home', 'action' => 'directory_siswa'),
        'before' => $this->Js->get('.data-siswa')->effect('fadeOut'),
        'complete' => $this->Js->get('.data-siswa')->effect('fadeIn')
    ));
?>
<center><h2>Data Siswa/i SMAN 2 Koto Kampar Hulu</h2></center>
<div class="center">
    <?php echo $this->Form->create('Siswa'); ?>
    <?php echo $this->Form->input('id_jurusan', array('div' => FALSE,"options" => $jurusan, "label" => "Jurusan", "empty" => "-- Jurusan --")) ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <?php echo $this->Form->input('nama', array('div' => FALSE)); ?>
    <?php echo $this->Js->submit(__('Cari', TRUE), array(
        'div' => FALSE,
        'update' => '.data-siswa', 
        'url' => array('controller' => 'Home', 'action' => 'search_siswa'),
        'before' => $this->Js->get('.data-siswa')->effect('fadeOut'),
        'complete' => $this->Js->get('.data-siswa')->effect('fadeIn')
    )); ?>
</div>
<div class="data-siswa">
    <table>
        <thead>
            <th>Nama</th>
            <th>Jurusan</th>
            <th>Tahun Masuk</th>
            <th>Aksi</th>
        </thead>
        <tbody>
            <?php if(sizeof($siswa) != 0) { ?>
                <?php foreach ($siswa as $siswa) : ?>
                    <tr class="light">
                        <td><?php echo $siswa['Siswa']['nama'] ?></td>
                        <td><?php echo $siswa['Jurusan']['nama_jurusan'] ?></td>
                        <td><?php echo $siswa['Siswa']['thn_masuk'] ?></td>
                        <td><?php echo $this->Js->link('Lihat Detail', array('controller' => 'Home', 'action' => 'view_siswa', base64_encode(base64_encode(base64_encode($siswa['Siswa']['id']))))) ?></td>
                    </tr>
                <?php endforeach; ?>
                <div class="center">
                    <?php echo $paginator->first(' First ', null, null, array('class' => 'disabled')); ?>
                    <?php echo $paginator->prev('Previous ', null, null, array('class' => 'disabled')); ?>
                    <?php echo $paginator->numbers(); ?>
                    <?php echo $paginator->next(' Next ', null, null, array('class' => 'disabled')); ?>
                    <?php echo $paginator->last(' Last ', null, null, array('class' => 'disabled')); ?>
                </div>
            <?php } else { ?>
                    <tr>
                        <td colspan="4">
                            <center>Data Siswa Tidak Tersedia</center>
                        </td>
                    </tr>
            <?php } ?>
        </tbody>
    </table>
</div>