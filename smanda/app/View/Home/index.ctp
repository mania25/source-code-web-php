<?php $paginator = $this->Paginator; ?>
<?php
    $this->Paginator->options(array(
        'update' => '.news',
        'evalScripts' => TRUE,
        'url' => array('controller' => 'Home', 'action' => 'index'),
        'before' => $this->Js->get('.news')->effect('fadeOut'),
        'complete' => $this->Js->get('.news')->effect('fadeIn')
    ));
?>
<!-- ###### -->
        <div id="left_column">
          <h2>Prospective Students</h2>
          <div class="imgholder"><a href="#"><img src="images/demo/220x80.gif" alt="" /></a></div>
          <h2>Current Students</h2>
          <div class="imgholder"><a href="#"><img src="images/demo/220x80.gif" alt="" /></a></div>
          <h2>International Students</h2>
          <div class="imgholder"><a href="#"><img src="images/demo/220x80.gif" alt="" /></a></div>
          <h2>Alumni</h2>
          <div class="imgholder"><a href="#"><img src="images/demo/220x80.gif" alt="" /></a></div>
        </div>
        <!-- ###### -->
        <div id="latestnews">
          <h2>Latest News &amp; Events</h2>
          <div class="news">
            <ul>
              <?php if(sizeof($berita) != 0) { ?>
                <?php foreach ($berita as $berita) : ?>
                      <li class="clear">
                        <div class="latestnews">
                            <p><?php echo $this->Html->link(ucfirst($berita['Berita']['judul']), array("action" => "detail_berita", $berita['Berita']['id'])) ?></p>
                            <p><?php echo substr($berita['Berita']['isi'], 0, 200) ?></p>
                        </div>
                      </li>
                <?php endforeach; ?>
                <p class="readmore">      
                  <?php echo $paginator->first(' First ', null, null, array('class' => 'disabled')); ?>
                  <?php echo $paginator->prev('Previous ', null, null, array('class' => 'disabled')); ?>
                  <?php echo $paginator->numbers(); ?>
                  <?php echo $paginator->next(' Next ', null, null, array('class' => 'disabled')); ?>
                  <?php echo $paginator->last(' Last ', null, null, array('class' => 'disabled')); ?>
                </p>
              <?php } else { ?>
                      <li class="clear">
                          <center><strong>Berita tidak tersedia.</strong></center>
                      </li>
              <?php } ?>
            </ul>
          </div>
        </div>
        <!-- ###### -->
        <div id="right_column">
          <div class="holder">
            <h2>Virtual Tour</h2>
            <a href="#"><img src="images/demo/video.gif" alt="" /></a> </div>
          <div class="holder">
            <h2>Quick Information</h2>
            <div class="apply"><a href="#"><img src="images/demo/100x100.gif" alt="" /> <strong>Make An Application</strong></a></div>
            <div class="apply"><a href="#"><img src="images/demo/100x100.gif" alt="" /> <strong>Order A Prospectus</strong></a></div>
          </div>
        </div>