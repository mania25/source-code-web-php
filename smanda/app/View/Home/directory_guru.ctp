<?php $paginator = $this->Paginator; ?>
<?php
    $this->Paginator->options(array(
        'update' => '.data-guru',
        'evalScripts' => TRUE,
        'url' => array('controller' => 'Home', 'action' => 'directory_guru'),
        'before' => $this->Js->get('.data-guru')->effect('fadeOut'),
        'complete' => $this->Js->get('.data-guru')->effect('fadeIn')
    ));
?>
<center><h2>Data Guru SMAN 2 Koto Kampar Hulu</h2></center>
<div class="center">
    <?php echo $this->Form->create('Guru'); ?>
    <?php echo $this->Form->input('jabatan', array('div' => FALSE, 'empty' => '-- Jabatan --', 'options' => array('Guru' => 'Guru'))); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <?php echo $this->Form->input('nama', array('div' => FALSE)); ?>
    <?php echo $this->Js->submit(__('Search', true), array(
        'div' => FALSE,
        'update' => '.data-guru', 
        'url' => array('controller' => 'Home', 'action' => 'search_guru',
        'before' => $this->Js->get('.data-guru')->effect('fadeOut'),
        'complete' => $this->Js->get('.data-guru')->effect('fadeIn')
            ))); ?>
    <?php echo $this->Form->end(NULL); ?>
    <br/>
</div>
<div class="data-guru">
    <table>
        <thead>
            <th>Nama</th>
            <th>Jabatan</th>
            <th>Pelajaran</th>
            <th>Aksi</th>
        </thead>
        <tbody>
            <?php if(sizeof($guru) != 0) { ?>
                <?php foreach ($guru as $guru) : ?>
                    <tr class="light">
                        <td><?php echo $guru['Guru']['nama'] ?></td>
                        <td><?php echo $guru['Guru']['jabatan'] ?></td>
                        <td><?php echo $guru['Guru']['pelajaran'] ?></td>
                        <td><?php echo $this->Form->postLink('Lihat Detail', array('controller' => 'Home', 'action' => 'view_guru', base64_encode(base64_encode(base64_encode($guru['Guru']['id']))))) ?></td>
                    </tr>
                <?php endforeach; ?>
                <div class="center">
                  <?php echo $paginator->first(' Awal ', null, null, array('class' => 'disabled')); ?>
                  <?php echo $paginator->prev('Sebelumnya ', null, null, array('class' => 'disabled')); ?>
                  <?php echo $paginator->numbers(); ?>
                  <?php echo $paginator->next(' Selanjutnya ', null, null, array('class' => 'disabled')); ?>
                  <?php echo $paginator->last(' AKhir ', null, null, array('class' => 'disabled')); ?>
                </div>
            <?php } else { ?>
                    <tr>
                        <td colspan="3">
                            <center>Data Guru Tidak Tersedia</center>
                        </td>
                    </tr>
            <?php } ?>
        </tbody>
    </table>     
</div>
