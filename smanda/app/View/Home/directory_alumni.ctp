<center><h2>Data Alumni SMAN 2 Koto Kampar Hulu</h2></center>
<table>
    <thead>
        <th>Nama</th>
        <th>Asal Universitas</th>
        <th>Tahun Lulus</th>
    </thead>
    <tbody>
        <?php if(sizeof($alumni) != 0) { ?>
            <?php foreach ($alumni as $alumni) : ?>
                <tr class="light">
                    <td><?php echo $alumni['Alumni']['nama'] ?></td>
                    <td><?php echo $alumni['Alumni']['kuliah_di'] ?></td>
                    <td><?php echo $alumni['Alumni']['thn_lulus'] ?></td>
                </tr>
            <?php endforeach; ?>
        <?php } else { ?>
                <tr>
                    <td colspan="3">
                        <center>Data Alumni Tidak Tersedia</center>
                    </td>
                </tr>
        <?php } ?>
    </tbody>
</table>