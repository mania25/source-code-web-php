<div class="alert alert-success fade" id="message">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <?php echo $message; ?>
</div>