<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppModel', 'Model');

/**
 * CakePHP Jurusan
 * @author mania25
 */
class Jurusan extends AppModel {
    public $useTable = 'jurusan';
    public $primaryKey = "id_jurusan";
    public $hasOne = array(
        'Siswa' => array(
            'className' => 'Siswa',
            'foreignKey' => 'id_jurusan'
        )
    );
}
