<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppModel', 'Model');

/**
 * CakePHP Siswa
 * @author mania25
 */
class Siswa extends AppModel {
    public $useTable = 'siswa';
    public $belongsTo = array(
        'Jurusan' => array (
            'className' => 'Jurusan',
            'foreignKey' => 'id_jurusan'
        ), 
        'Kelas' => array (
            'className' => 'Kelas',
            'foreignKey' => 'id_kelas'
        )
    );
}
