<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppModel', 'Model');

/**
 * CakePHP User
 * @author mania25
 */
class User extends AppModel {
    public $useTable = 't@b3!_u53rn4m3';
    public $hasMany = array(
        'Berita' => array(
            'className' => 'Berita',
            'foreignKey' => 'id_author',
            'dependent' => true
        )
    );
}
