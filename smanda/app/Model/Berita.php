<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppModel', 'Model');

/**
 * CakePHP Berita
 * @author mania25
 */
class Berita extends AppModel {
    public $useTable = 'berita';
    public $belongsTo = array(
        'User' => array (
            'classname' => 'User',
            'foreignKey' => 'id_author'
        )
    );
    public $validate = array(
        'judul' => array(
            'rule' => 'notEmpty'
        ), 
        'isi' => array(
            'rule' => 'notEmpty'
        )
    );
}
