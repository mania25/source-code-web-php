<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppModel', 'Model');

/**
 * CakePHP Kelas
 * @author mania25
 */
class Kelas extends AppModel {
    public $useTable = 'kelas';
    public $primaryKey = "id_kelas";
    public $hasOne = array(
        'Siswa' => array(
            'className' => 'Siswa',
            'foreignKey' => 'id_kelas'
        )
    );
}
