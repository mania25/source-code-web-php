$(function() {

    $('#side-menu').metisMenu();

});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
    $(window).bind("load resize", function() {
        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        height = (this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });
});

$("#message").delay(600).addClass("in").fadeOut(3500);

        
$('.edit').click(function(){
		var id = $(this).parent().prev().prev().prev().prev().prev().text();
		var judul = $(this).parent().prev().prev().prev().prev().text();
                var isi = $(this).parent().prev().prev().prev().text();
                
                $('.modal-content #id').val(id);
        	$('.modal-content #judul').val(judul);
                CKEDITOR.instances['isi'].setData(isi);
        	$('modal-user').modal({show:true});
    	});
        

$('.view').click(function(){
		var judul = $(this).parent().prev().prev().prev().prev().text();
                var isi = $(this).parent().prev().prev().prev().text();
        	$('.modal-content #judul').val(judul);
        	$('.modal-content #isi').val(isi);
        	$('modal-user').modal({show:true});
    	});
        
$('.tambah-guru').click(function(){
                $('.modal-content #ttl').attr('type', 'date');
        	$('modal-user').modal({show:true});
    	});
        
        
$('.tambah-siswa').click(function(){
                $('.modal-content #tanggal_lahir').attr('type', 'date');
        	$('modal-user').modal({show:true});
    	});
        
$('.edit-guru').click(function(){
		var id = $(this).parent().prev().prev().prev().prev().prev().prev().prev().prev().text();
		var nip = $(this).parent().prev().prev().prev().prev().prev().prev().prev().text();
		var nama = $(this).parent().prev().prev().prev().prev().prev().prev().text();
		var kota_lahir = $(this).parent().prev().prev().prev().prev().prev().text();
		var ttl = $(this).parent().prev().prev().prev().prev().text();
		var jabatan = $(this).parent().prev().prev().prev().text();
		var pendidikan = $(this).parent().prev().prev().text();
		var mapel = $(this).parent().prev().text();
                
                $('.modal-content #ttl').attr('type', 'date');
                $('.modal-content #id').val(id);
                $('.modal-content #nip').val(nip);
        	$('.modal-content #nama').val(nama);
                $('.modal-content #kota_lahir').val(kota_lahir);
                $('.modal-content #ttl').val(ttl);
        	$('.modal-content #mapel').val(mapel);
                $('.modal-content #pendidikan_terakhir').val(pendidikan);
        	$('.modal-content #jabatan').val(jabatan);
        	$('modal-user').modal({show:true});
    	});
        

$('.view-guru').click(function(){
		var nip = $(this).parent().prev().prev().prev().prev().prev().prev().prev().text();
		var nama = $(this).parent().prev().prev().prev().prev().prev().prev().text();
		var kota_lahir = $(this).parent().prev().prev().prev().prev().prev().text();
		var ttl = $(this).parent().prev().prev().prev().prev().text();
		var jabatan = $(this).parent().prev().prev().prev().text();
		var pendidikan = $(this).parent().prev().prev().text();
		var mapel = $(this).parent().prev().text();
                
                $('.modal-content #ttl').attr('type', 'date');
                $('.modal-content #nip').val(nip);
        	$('.modal-content #nama').val(nama);
                $('.modal-content #kota_lahir').val(kota_lahir);
                $('.modal-content #ttl').val(ttl);
        	$('.modal-content #mapel').val(mapel);
                $('.modal-content #pendidikan_terakhir').val(pendidikan);
        	$('.modal-content #jabatan').val(jabatan);
        	$('modal-user').modal({show:true});
    	});
        
        
$('.view-siswa').click(function(){
		var nis = $(this).parent().prev().prev().prev().prev().prev().prev().prev().prev().prev().text();
		var nama = $(this).parent().prev().prev().prev().prev().prev().prev().prev().prev().text();
		var kota_lahir = $(this).parent().prev().prev().prev().prev().prev().prev().prev().text();
		var tanggal_lahir = $(this).parent().prev().prev().prev().prev().prev().prev().text();
		var alamat = $(this).parent().prev().prev().prev().prev().prev().text();
		var jenis_kelamin = $(this).parent().prev().prev().prev().prev().text();
		var jurusan = $(this).parent().prev().prev().prev().text();
		var kelas = $(this).parent().prev().prev().text();
		var tahun_masuk = $(this).parent().prev().text();
                
                $('.modal-content #tanggal_lahir').attr('type', 'date');
                $('.modal-content #nis').val(nis);
        	$('.modal-content #nama').val(nama);
                $('.modal-content #kota_lahir').val(kota_lahir);
                $('.modal-content #tanggal_lahir').val(tanggal_lahir);
                $('.modal-content #alamat').val(alamat);
        	$('.modal-content #jk').val(jenis_kelamin);
                if (jurusan === "IPA"){
                    $('.modal-content #jurusan').val("1");
                } else {
                    $('.modal-content #jurusan').val("2");
                }
        	$('.modal-content #kelas').val(kelas);
        	$('.modal-content #tahun_masuk').val(tahun_masuk);
        	$('modal-user').modal({show:true});
    	});
        
        
$('.edit-siswa').click(function(){
		var id = $(this).parent().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().text();
		var nis = $(this).parent().prev().prev().prev().prev().prev().prev().prev().prev().prev().text();
		var nama = $(this).parent().prev().prev().prev().prev().prev().prev().prev().prev().text();
		var kota_lahir = $(this).parent().prev().prev().prev().prev().prev().prev().prev().text();
		var tanggal_lahir = $(this).parent().prev().prev().prev().prev().prev().prev().text();
		var alamat = $(this).parent().prev().prev().prev().prev().prev().text();
		var jenis_kelamin = $(this).parent().prev().prev().prev().prev().text();
		var jurusan = $(this).parent().prev().prev().prev().text();
		var kelas = $(this).parent().prev().prev().text();
		var tahun_masuk = $(this).parent().prev().text();
                
                $('.modal-content #tanggal_lahir').attr('type', 'date');
                $('.modal-content #id').val(id);
                $('.modal-content #nis').val(nis);
        	$('.modal-content #nama').val(nama);
                $('.modal-content #kota_lahir').val(kota_lahir);
                $('.modal-content #tanggal_lahir').val(tanggal_lahir);
                $('.modal-content #alamat').val(alamat);
        	$('.modal-content #jk').val(jenis_kelamin);
                if (jurusan === "IPA"){
                    $('.modal-content #jurusan').val("1");
                } else {
                    $('.modal-content #jurusan').val("2");
                }
        	$('.modal-content #kelas').val(kelas);
        	$('.modal-content #tahun_masuk').val(tahun_masuk);
        	$('modal-user').modal({show:true});
    	});