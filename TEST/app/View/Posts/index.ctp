<h1>Blog posts</h1>
<?php
    $this->Paginator->options(array(
        'update' => '.post',
        'evalScripts' => TRUE,
        'url' => array('controller' => 'posts', 'action' => 'index'),
        'before' => $this->Js->get('.post')->effect('fadeOut'),
        'complete' => $this->Js->get('.post')->effect('fadeIn')
    ));
?>
<p><?php $paginator = $this->Paginator; echo $this->Html->link('Add Post', array('action' => 'add')); ?></p>
<?php echo $this->Form->create('Post'); ?>
<?php echo $this->Form->input('title', array('id' => 'judul')); ?>
<?php echo $this->Form->end(array('id' => 'submit')); ?>
<div class="loading-ajax" style="display: none">
    <p>Loading data . . .</p>
</div>
<div class="post">
<table>
    <tr>
        <th>Id</th>
        <th>Title</th>
        <th>Actions</th>
        <th>Created</th>
    </tr>

<!-- Here's where we loop through our $posts array, printing out post info -->

    <?php foreach ($posts as $post): ?>
    <tr>
        <td><?php echo $post['Post']['id']; ?></td>
        <td>
            <?php
                echo $this->Html->link(
                    $post['Post']['title'],
                    array('action' => 'view', $post['Post']['id'])
                );
            ?>
        </td>
        <td>
            <?php
                echo $this->Form->postLink(
                    'Delete',
                    array('action' => 'delete', $post['Post']['id']),
                    array('confirm' => 'Are you sure?')
                );
            ?>
            <?php
                echo $this->Html->link(
                    'Edit', array('action' => 'edit', $post['Post']['id'])
                );
            ?>
        </td>
        <td>
            <?php echo $post['Post']['created']; ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>    
        <?php echo $paginator->first(' First ', null, null, array('class' => 'disabled')); ?>
        <?php echo $paginator->prev('Previous ', null, null, array('class' => 'disabled')); ?>
        <?php echo $paginator->numbers(); ?>
       <?php echo $paginator->next(' Next ', null, null, array('class' => 'disabled')); ?>
        <?php echo $paginator->last(' Last ', null, null, array('class' => 'disabled')); ?>
</div>
<?php
    $formData = $this->Js->get('#judul')->serializeForm(array('isForm' => TRUE, 'inline' => TRUE));
    $this->Js->get('#judul')->event('change', $this->Js->request(
                array('controller' => 'posts', 'action' => 'search'),
                array(
                    'update' => '.post',
                    'data' => $formData,
                    'async' => TRUE,
                    'dataExpression' => TRUE,
                    'method' => 'POST'
                    )
            ));
?>